job "mariadb" {
	datacenters = ["dc1"]
	type = "service"
	group "mariadb" {
		network {
			port "db" {
				static = 3306
				to = 3306
			}
		}
		service {
			name = "mariadb"
			tags = ["db"]
			port = "3306"
			# Requires password login
			// check {
			//     type = "script"
			//     name = "Check All DBs"
			//     command = "mariadb-check"
			//     args = ["--all-databases"]
			//     interval = "10s"
			//     timeout = "1s"
			//     task = "mariadb"
			// }
		}
		task "mariadb" {
			vault {
				policies = ["mariadb"]
			}
			resources {
				cpu = 1000
				memory = 1000
			}
			template {
				data        = <<EOH
{{ with secret "pki_int/cert/ca_chain" }}{{ .Data.certificate }}
{{ end }}
EOH
				destination = "local/cachain.pem"
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=mariadb" "common_name=mariadb.service.consul" "ttl=24h" "alt_names=_mariadb._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.certificate }}
{{ end }}{{ end }}
EOH
				destination = "local/cert.pem"
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=mariadb" "common_name=mariadb.service.consul" "ttl=24h" "alt_names=_mariadb._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}
EOH
				destination = "secrets/key.pem"
			}
			template {
				data =<<EOH
[client-server]
# Port or socket location where to connect
# port = 3306
socket = /run/mysqld/mysqld.sock

[mariadb]
ssl_cert = /local/cert.pem
ssl_key = /secrets/key.pem
ssl_ca = /local/cachain.pem
require-secure-transport = on

# Import all .cnf files from configuration directory
[mariadbd]
skip-host-cache
skip-name-resolve
# plugin-load=auth_pam.so

!includedir /etc/mysql/mariadb.conf.d/
!includedir /etc/mysql/conf.d/
EOH
				destination = "local/etc/mysql/mariadb.cnf"
			}
			template {
				data = <<EOH
				MARIADB_ROOT_PASSWORD="password"
				EOH
				destination = "secrets/file.env"
				env = true
			}
			driver = "docker"
			config {
				image = "mariadb"
				ports = ["db"]
				volumes = ["local/etc/mysql/mariadb.cnf:/etc/mysql/mariadb.cnf"]
			}
			volume_mount {
				volume = "data"
				destination = "/var/lib/mysql"
			}
		}
		volume "data" {
			type = "host"
			source = "mariadb"
			read_only = false
		}
	}
}
